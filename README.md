## Introduction

This tutorial aims to provide a step by step installation instruction for using Intel Realsense driver sensors (L515, T265, D4XX, etc.) on ROS.�There are two major components: librealsense and realsense-ros. You need to install librealsense first.

## Install librealsense on Intel CPU core

Official instraction can be found by:
.[librealsense](https://github.com/IntelRealSense/librealsense/blob/master/doc/distribution_linux.md).

It is for installation via the Linux APT management tool, if you encounter any problem with such an installation method, you can try building from the source code by following this:
.[librealsense-source](https://github.com/IntelRealSense/librealsense/blob/master/doc/installation.md).

You need to uninstall all the realsense software package before building from the source by:
````
dpkg -l | grep "realsense" | cut -d " " -f 3 | xargs sudo dpkg --purge
````

**Key steps:**

Register the server's public key:
````
sudo apt-key adv --keyserver keys.gnupg.net --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE || sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE
````
Add the server to the list of repositories:

Ubuntu 16 LTS:
````
sudo add-apt-repository "deb http://realsense-hw-public.s3.amazonaws.com/Debian/apt-repo xenial main" -u
````

Ubuntu 18 LTS:
````
sudo add-apt-repository "deb http://realsense-hw-public.s3.amazonaws.com/Debian/apt-repo bionic main" -u
````

Install the libraries (see section below if upgrading packages):
````
sudo apt-get install librealsense2-dkms
sudo apt-get install librealsense2-utils
````

## Install librealsense on TX2

Download librealsense:
.[librealsense_tx2](git clone https://github.com/IntelRealSense/librealsense.git)

**Go to the downloaded librealsense folder, modify file ./scripts/patch-xhci-realsense-ubuntu-xenial.sh as follow:**

Replace kernel_name="ubuntu-xenial" (line #28) with 
````
kernel_name="kernel-4.4"
````

Replace the source tree (just one line below to the kernel_name, line #30-31 on Github) with: 
````
[ ! -d ${kernel_name} ] && git clone https://github.com/jetsonhacks/buildJetsonTX2Kernel.git && cd buildJetsonTX2Kernel && ./getKernelSources.sh && ./scripts/fixMakeFiles.sh && cd .. && cp /usr/src/kernel/${kernel_name} ./${kernel_name}
````
Comment out the if-fi lines (just below the source tree, line #34-52 on Github) as follow:
````
#if [ $(git status | grep 'modified:' | wc -l) -ne 0 ];
#then
#	echo -e "\e[36mThe kernel has modified files:\e[0m"
#	git status | grep 'modified:'
#	echo -e "\e[36mProceeding will reset all local kernel changes. Press 'n' within 10 seconds to abort the procedure"
#	read -t 10 -r -p "Do you want to proceed? [Y/n]" response
#	response=${response,,}    # tolower
#	if [[ $response =~ ^(n|N)$ ]]; 
#	then
#		echo -e "\e[41mScript has been aborted on user requiest. Please resolve the modified files are rerun\e[0m"
#		exit 1
#	else
#		echo -e "\e[0m"
#		printf "Resetting local changes in %s folder\n " ${kernel_name}
#		git reset --hard
#		echo -e "\e[32mUpdate the folder content with the latest from mainline branch\e[0m"
#		git pull origin master
#	fi
#fi
````

**Key steps:**
Update and upgrade Ubuntu kernel 
````
sudo apt-get update && sudo apt-get upgrade && sudo apt-get dist-upgrade
sudo update-grub && sudo reboot
````

Install the core packages required to build librealsense binaries and the affected kernel modules:
````
sudo apt-get install git libssl-dev libusb-1.0-0-dev pkg-config libgtk-3-dev
````

Ubuntu 16:
````
sudo apt-get install libglfw3-dev
````

Ubuntu 18:
````
sudo apt-get install libglfw3-dev libgl1-mesa-dev libglu1-mesa-dev
````

Install Intel Realsense permission scripts located in librealsense source directory:
````
sudo cp config/99-realsense-libusb.rules /etc/udev/rules.d/
sudo udevadm control --reload-rules && udevadm trigger
````

Run installization script
````
./scripts/patch-xhci-realsense-ubuntu-xenial.sh
````

Navigate to librealsense root directory and run:
````
mkdir build && cd build
````

Builds librealsense along with the demos and tutorials
````
cmake ../ -DBUILD_EXAMPLES=true
````

Recompile and install librealsense binaries:
````
sudo make uninstall && make clean && make && sudo make install
````

## Install realsense-ros

Official instruction can be found by:
.[realsense-ros](https://github.com/IntelRealSense/realsense-ros)

